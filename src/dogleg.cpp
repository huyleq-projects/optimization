
/*
 * Reference: http://www2.imm.dtu.dk/pubdb/edoc/imm3215.pdf
 */


#include <cstring>
#include <limits>

#include <cblas.h>
#include <lapacke.h>
#include <glog/logging.h>

#include "dogleg.h"

void dogleg(const DoglegOptions &options,
            const int m, const int n, double *x,
            const std::function<double(const int, const int, const double *,
                                double *, double *, double *)> &func){
    LOG_IF(FATAL, m < n) <<"FATAL: underdetermined problem";

    double delta = options.Delta; // initial trust region radius
    double tolf = options.Tolf; // |df/f| <= tolf
    double tolg = options.Tolg; // |g|_inf <= tolg
    double tolx = options.Tolx; // |xnew-x| <= tolx(|x|+tolx)
    double tolr = options.Tolr; // |r|_inf <= tolr
    int maxeval = options.Maxeval; // max # of evaluations
    
    double nx = cblas_dnrm2(n, x, 1); // |x|
    if(delta <= 0) delta = 0.1*(1+nx); // if no delta supplied

    int mn = m*n;
    double *g = new double[n]; // gradient
    double *r = new double[m]; // residual
    double *J = new double[mn]; // jacobian
    
    double *h_gn = new double[n]; // gn step
    double *h_sd = new double[n]; // steepest descent step
    double *h_dl = new double[n]; // dogleg step
    double *temp = new double[m]; // temp array

    int stop=0, iter=0, neval=0;
    double nu=2, fold = std::numeric_limits<double>::max();

    printf("iter  neval      cost       |r|_inf     |g|_inf    tr_radius\n");
    const char* format = "% 4d  % 4d % 8e   % 3.2e   % 3.2e  % 3.2e\n";

    while(!stop){
        double tolnh = tolx*(nx+tolx); // tol for |h|
        double f = func(m, n, x, r, g, J); // current f
        double rdf = std::abs((fold - f)/fold); // relative change in f
        fold = f; // update fold
        double ng_inf = std::abs(g[cblas_idamax(n, g, 1)]); // |g|_inf
        double nr_inf = std::abs(r[cblas_idamax(m, r, 1)]); // |r|_inf
        iter++; neval++;

        printf(format, iter, neval, f, nr_inf, ng_inf, delta);
        LOG(INFO) << std::endl;
        LOG(INFO) << "iter = " << iter << " f = " << f << " |r|_inf = " << nr_inf
                    << " |g|_inf = " << ng_inf << " delta = " << delta;

        // check convergence
        if(rdf <= tolf){
            stop = 1; 
            printf("STOP: |df/f| too small\n");
            break;
        }
        else if(ng_inf <= tolg){
            stop = 2; 
            printf("STOP: |g|_inf too small\n");
            break;
        }
        else if(delta <= tolnh){
            stop = 3; 
            printf("STOP: tr radius too small\n");
            break;
        }
        else if(nr_inf <= tolr){
            stop = 4; 
            printf("STOP: |r|_inf too small\n");
            break;
        }
        else if(neval >= maxeval){
            stop = 5; 
            printf("STOP: neval exceeds maxeval (%d)\n", maxeval);
            break;
        }

        // steepest descent 
        double g2 = cblas_ddot(n, g, 1, g, 1); // |g|^2
        cblas_dgemv(CblasRowMajor, CblasNoTrans, m, n, 1, J, n, g, 1, 0, temp, 1); // J*g
        double Jg2 = cblas_ddot(m, temp, 1, temp, 1); // |Jg|^2 
        double alpha = g2/Jg2; // alpha = |g|^2/|Jg|^2
        
        cblas_daxpby(n, -alpha, g, 1, 0, h_sd, 1); // h_sd = -alpha*g
        double ng = sqrt(g2); // |g|
        double nh_sd = alpha*ng; // |h_sd|
        LOG(INFO) << "|nh_sd| = " << nh_sd;

        // gauss-newton
        compute_gn_step(m, n, h_gn, r, J); // J*h_gn = -r
        double nh_gn = cblas_dnrm2(n, h_gn, 1); // |h_gn|
        LOG(INFO) << "|nh_gn| = " << nh_gn;

        if(nh_gn <= tolnh){
            stop = 3; // |h_gn| <= tolx*(|x|+tolx), ie, gn step too small
            printf("STOP: |h_gn| too small\n");
            break;
        }

        while(delta > tolnh){
            // compute dogleg step
            double dL = compute_dogleg_step(n, h_dl, h_sd, nh_sd, h_gn, nh_gn, f, g2, delta, alpha);
            double nh_dl = cblas_dnrm2(n, h_dl, 1);
            LOG(INFO) << "|nh_dl| = " << nh_dl;

            if(nh_dl <= tolnh){
                stop = 2; // |h_dl| <= tolx*(|x|+tolx), ie, step too small
                printf("STOP: |h_dl| too small\n");
                break;
            }
            else{
                cblas_daxpy(n, 1, x, 1, h_dl, 1); // h_dl -> xnew = x+h_dl;
                double fnew = func(m, n, h_dl, temp, nullptr, nullptr); // f(xnew)
                neval++;
                double df = f-fnew; // decrease in f
                LOG(INFO) << "Testing new x: df = " << df << " dL = " << dL;

                if(df > 0 && dL > 0){ // accept new x
                    cblas_dcopy(n, h_dl, 1, x, 1); // update x = xnew
                    nx = cblas_dnrm2(n, x, 1); // update |x|
                    double rho = df/dL; // quality of linear model and step

                    LOG(INFO) << "Accepting new x: rho = " << rho;

                    if(0.8 < rho && rho < 1.2 && 3*nh_dl > delta){
                        LOG(INFO) << "good model, increasing delta by 3";
                        delta *= 3; nu=2; // try larger delta next interation
                    }
                    else if(rho < 0.25){
                        LOG(INFO) << "not so good model, decreasing delta by: nu = " << nu;
                        delta /= nu; nu *= 2; // reduce delta
                    }
                    break;
                }
                else{
                    LOG(INFO) << "not so good new x, decreasing delta by: nu = " << nu;
                    delta /= nu; nu *= 2; // reduce delta
                }
            }
        }
    }
    
    google::FlushLogFiles(google::INFO);
    
    delete []g; delete []r; delete []J; delete []temp;
    delete []h_gn; delete []h_sd; delete []h_dl;
    return;
}

// compute GN step J^T*J*h = -J^T*r
// by solving least square J*h = r and negate h
void compute_gn_step(const int m, const int n, double *h_gn, double *r, double *J){
    // solve J*h = r by truncated SVD
    lsolve_svd(m, n, h_gn, J, r);

    // negation since we want R*h = -Q^T*r
    cblas_dscal(n, -1, h_gn, 1);

    return;
}

// solve A*x = b by SVD dropping small singular values
void lsolve_svd(const int m, const int n, double *x, double *a, double *b){
    // use built-in lapacke function
//    double *s = new double[n];
//    double rcond= 100*std::numeric_limits<double>::epsilon();
//    int rank;
//    
//    LAPACKE_dgelss(LAPACK_ROW_MAJOR, m, n, 1, a, n, b, m, s, rcond, &rank);
//    cblas_dcopy(n, b, 1, x, 1);
//    printf("x1 %g x2 %g\n", x[0], x[1]);
//    
//    LOG(INFO) << "singular values: min = " << s[n-1] << " max = " << s[0];
//    LOG(INFO) << "condition number = " << s[0]/s[n-1];
//    LOG(INFO) << "choosing " << rank << " singular values out of " << n;
//    
//    delete []s;

    const int mn = m*n, nn = n*n;
    double *u = new double[mn];
    double *vt = new double[nn];
    double *s = new double[n];
    double *superb=new double[n-1];
    
    // A = U*S*Vt
    LAPACKE_dgesvd(LAPACK_ROW_MAJOR, 'S', 'S', m, n, a, n, s, u, n, vt, n, superb);
    
    // lower bound for sigma
    double tols = 100*std::numeric_limits<double>::epsilon()*s[0];
    int k; // # of acceptable sigmas
    for(k=0; k<n; k++){
        if(s[k] <= tols) break;
    }
    LOG(INFO) << "singular values: min = " << s[n-1] << " max = " << s[0];
    LOG(INFO) << "condition number = " << s[0]/s[n-1];
    LOG(INFO) << "singular value tol = " << tols;
    LOG(INFO) << "choosing " << k << " singular values out of " << n;
    
    // solve U*S*Vt*x = b, keeping only k singular values
    // x -> U^T*b 
    cblas_dgemv(CblasRowMajor, CblasTrans, m, k, 1, u, n, b, 1, 0, x, 1);
    
    // b -> S^-1*x
    for(int i=0; i<k; i++) b[i] = x[i]/s[i];

    // x -> V*b 
    cblas_dgemv(CblasColMajor, CblasNoTrans, n, k, 1, vt, n, b, 1, 0, x, 1);
    printf("x1 %g x2 %g\n", x[0], x[1]);

    delete []u; delete []vt; delete []s; delete []superb;
    return;
}

double compute_dogleg_step(const int n, double *h_dl, 
                           const double *h_sd, const double nh_sd,
                           const double *h_gn, const double nh_gn, 
                           const double f, const double g2,
                           const double delta, const double alpha){
    double dL;
    if(nh_gn <= delta){ // take GN step
        LOG(INFO) << "Taking gn step";
        cblas_dcopy(n, h_gn, 1, h_dl, 1); // h_dl = h_gn
        dL = f; // L(0)-L(h) = f
    }
    else if(nh_sd >= delta){ // reduce and take steepest descent step
        LOG(INFO) << "Taking sd step";
        cblas_daxpby(n, delta/nh_sd, h_sd, 1, 0, h_dl, 1); // h_dl = (delta/|h_sd|)*h_sd
        dL = delta*(2*nh_sd - delta)/(2*alpha);
    }
    else{ // dogleg
        LOG(INFO) << "Taking dogleg step";
        cblas_dcopy(n, h_sd, 1, h_dl, 1); // h_dl = h_sd
        cblas_daxpby(n, 1, h_gn, 1, -1, h_dl, 1); // h_dl = b-a = h_gn-h_sd;
        double b_a2 = cblas_ddot(n, h_dl, 1, h_dl, 1); // |b-a|^2
        double c = cblas_ddot(n, h_sd, 1, h_dl, 1); // c = aT*(b-a)
        double c2 = c*c; // c^2
        double delta2_a2 = delta*delta - nh_sd*nh_sd; // delta^2-|a|^2
        
        double beta;
        if(c <= 0) beta = (sqrt(c2 + b_a2*delta2_a2) - c)/b_a2;
        else beta = delta2_a2/(sqrt(c2 + b_a2*delta2_a2) + c);
        
        cblas_daxpby(n, 1, h_sd, 1, beta, h_dl, 1); // h_dl = alpha*h_sd+beta*(h_gn-h_sd);
        dL = 1-beta;
        dL = alpha*dL*dL*g2/2 + beta*(2-beta)*f;
    }
    return dL;
}
