#ifndef SIMPLEX_H
#define SIMPLEX_H

/*
 x m+n array that stores nonbasic variables from 0 to n-1, 
 and basic variables from n to m+n
 a m by n matrix column major that stores constraint coefficients
 b m array that stores constraint coefficients
 c n+1 array that stores objective function coefficients,
 c[n] stores the free coefficient
 p m+n array that stores the indices of the original variables
 p[0] to p[n-1] are the basic variables' indices
 p[n] to p[m+n-1] are the nonbasic variables' indices
 m number of constraints
 n number of variables
 l leaving index, 0 <= l < m
 e entering index, 0 <= e < n
 */

// perform 1 pivot operation
void pivot(double *a, double *b, double *c, int *p, int m, int n, int l, int e);

/*
 find feasible basic solution
 return value: 1 found feasible basic solution
               0 infeasible
              -2 error 
 */
int init_simplex(double *a, double *b, double *c, int *p, int m, int n);

/*
 solve for optimal solution starting with feasbile basic solution
 return value: 1 found optimal solution
              -1 unbounded
 */
int solve_simplex(double *a, double *b, double *c, int *p, int m, int n);

/*
 main caller for simplex algorithm 
 return value: 1 found optimal solution
               0 infeasible
              -1 unbounded
              -2 error 
 */
int simplex(double *x, double *a, double *b, double *c, int m, int n);

#endif
