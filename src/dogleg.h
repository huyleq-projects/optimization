#ifndef DOGLEG_H
#define DOGLEG_H

#include <cmath>
#include <functional>

struct DoglegOptions{
    double Delta=0, Tolf=1e-10, Tolg=1e-14, Tolx=1e-18, Tolr=1e-16;
    int Maxeval=100;
};

void dogleg(const DoglegOptions &options,
            const int m, const int n, double *x,
            const std::function<double(const int, const int, const double *,
                                double *, double *, double *)> &func);

void compute_gn_step(const int m, const int n, double *h_gn, double *r, double *J);

void lsolve_svd(const int m, const int n, double *x, double *a, double *b);

double compute_dogleg_step(const int n, double *h_dl, 
                           const double *h_sd, const double nh_sd,
                           const double *h_gn, const double nh_gn, 
                           const double f, const double g2,
                           const double delta, const double alpha);

#endif
