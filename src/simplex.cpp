#include <cstdio>
#include <cstring>
#include <limits>
#include <algorithm>

#include <cblas.h>

#include "util.h"
#include "simplex.h"

void pivot(double *a, double *b, double *c, int *p, int m, int n, int l, int e){
    // modify the l'th constraint
    double ale = a[l+e*m]; b[l] /= ale; a[l+e*m] = 1./ale;
    for(int j = 0; j < n; j++){
        if(j != e) a[l+j*m] /= ale;
    }

    // modify the other m-1 constraints
    for(int i = 0; i < m; i++){
        if(i != l){
            double aie = a[i+e*m]; b[i] -= aie*b[l];
            for(int j = 0; j < n; j++){
                if(j != e) a[i+j*m] -= aie*a[l+j*m];
            }
            a[i+e*m] = -aie*a[l+e*m];
        }
    }

    // modify the objective function
    double ce = c[e]; c[n] += ce*b[l]; c[e] = -ce*a[l+e*m];
    for(int j = 0; j < n; j++){
        if(j != e) c[j] -= ce*a[l+j*m];
    }

    // modify the index array p by switching p[e] and p[l+n]
    int lpn = l+n, temp = p[e]; p[e] = p[lpn]; p[lpn] = temp;
    return;
}

// check if basic solution is feasible
bool is_basic_feasible(const double *b, int m){
    for(int i = 0; i < m; i++){
        if(b[i] < 0) return false;
    }
    return true;
}

// find index of original variable xk in current slack form
int find_variable_index(const int *p, int mpn, int k){
    for(int i = 0; i < mpn; i++){
        if(p[i] == k) return i;
    }
    return -1;
}

// initialize with feasible basic solution
int init_simplex(double *a, double *b, double *c, int *p, int m, int n){
    int k = cblas_idmin(m, b, 1);
    if(b[k] >= 0){ // initial basic solution is feasible
        int mpn = m + n;
        for(int i = 0; i < mpn; i++) p[i] = i; 
        return 1;
    }
    
    // form the auxilary LP with m constraints and n+1 variables
    int mm = m, nn = n+1, mmpnn = mm+nn, nnp1 = nn+1;
    
    // add -x0 to the lhs of Ax <= b
    double *aa = new double[mm*nn];
    for(int i = 0; i < m; i++) aa[i] = -1;
    std::memcpy(aa+m, a, m*n*sizeof(double));

    double *bb = b; // reuse b for both problems

    // objective function of aux LP is -x0
    double *cc = new double[nnp1];
    std::memset(cc, 0, nnp1*sizeof(double)); cc[0] = -1;
    
    // set the index array of aux LP
    int *pp = new int[mmpnn];
    for(int i = 0; i < mmpnn; i++) pp[i] = i; 
    
    // perform 1 pivot to get feasible basic solution of aux LP
    pivot(aa, bb, cc, pp, mm, nn, k, 0);

    int r;
    if(!is_basic_feasible(bb, mm)){
        printf("basic solution is not feasible for aux LP. something is wrong\n");
        r = -2;
    }
    
    if(r != -2){ // now that the basic solution is feasible, solve aux LP
        r = solve_simplex(aa, bb, cc, pp, mm, nn);
    
        if(r == 1 && cc[nn] == 0){ // aux LP solved with optimal value -x0=0
            int k = find_variable_index(pp, mmpnn, 0); // index of x0
            if(k >= nn){ // x0 is basic in optimal solution
                int l = k - nn, e = 0;
                while(e < nn && aa[l+e*m] == 0) e++;
                pivot(aa, bb, cc, pp, mm, nn, l, e); // to make x0 nonbasic
                k = e;
            }

            // remove x0 from A, i.e. remove column k'th from aux LP's A
            std::memcpy(a, aa, k*m*sizeof(double));
            std::memcpy(a, aa+(k+1)*m, (nn-k-1)*m*sizeof(double));
    
            // remove x0 from aux LP's p to form p for the original LP
            for(int i = 0; i < k; i++) p[i] = pp[i] - 1;
            for(int i = k+1; i < mmpnn; i++) p[i-1] = pp[i] - 1;
            
            // modify the original objective function
            std::memcpy(cc, c, n*sizeof(double)); // make temp copy of c
            std::memset(c, 0, n*sizeof(double)); // set c = 0

            for(int i = 0; i < n; i++){ // loop over all ci*xi term
                if(cc[i] != 0){
                    k = find_variable_index(p, m+n, i); // index of xi in slack form
                    if(k < n) c[k] += cc[i]; // xi is nonbasic
                    else{ // xi is basic, use constraint (k-n)'th to sub for xi
                        int kn = k - n; c[n] += cc[i]*b[kn]; 
                        for(int j = 0; j < n; j++) c[j] -= cc[i]*a[kn+j*m];
                    }
                }
            }
        }
        else r = 0; // original LP is infeasible
    }

    delete []aa; delete []cc; delete []pp;
    return r;
}

// find entering variable, which has ci > 0 and smallest index in the original LP
int entering_index(const double *c, const int *p, int m, int n){
    int e = -1, k = m + n;
    for(int i = 0; i < n; i++){
        if(c[i] > 0 && p[i] < k){
            e = i; k = p[i];
        }
    }
    return e;
}

// find leaving variable, which is most constrained 
// and has smallest index in the original LP
int leaving_index(const double *a, const double *b, const int *p, int m, int n){
    int l = -1, k = m + n;
    double mm = std::numeric_limits<double>::max(), t;
    for(int i = 0; i < m; i++){
        if(a[i] > 0 && (t = b[i]/a[i]) <= mm){
            if(t < mm || p[i+n] < k){
                mm = t; l = i; k = p[i+n];
            }
        }
    }
    return l;
}

// solve for optimal solution starting with feasible basic solution
int solve_simplex(double *a, double *b, double *c, int *p, int m, int n){
    int e;
    while((e = entering_index(c, p, m, n)) >= 0){
        int l = leaving_index(a+e*m, b, p, m, n);
        if(l < 0) return -1; // unbounded
        pivot(a, b, c, p, m, n, l, e);
    }
    return 1;
}

// main caller for simplex
int simplex(double *x, double *a, double *b, double *c, int m, int n){
    int mpn = m + n; int *p = new int[mpn];
    
    // find feasible basic solution
    int r = init_simplex(a, b, c, p, m, n);

    if(r == 1){ // found a feasible basic solution
        if(!is_basic_feasible(b, m)){ // double check
            printf("initial basic solution is not feasible. something is wrong\n");
            r = -2;
        }
        else{ // solve starting with feasible basic solution
            r = solve_simplex(a, b, c, p, m, n);
    
            if(r == 1){ // found optimal solution 
                for(int i = 0; i < n; i++){ // nonbasic variables
                    if(p[i] < n) x[p[i]] = 0;
                }
                for(int i = n; i < mpn; i++){ // basic variables
                    if(p[i] < n) x[p[i]] = b[i-n];
                }
            }
        }
    }
    
    delete []p;
    return r;
}
