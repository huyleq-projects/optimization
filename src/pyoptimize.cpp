#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/functional.h>

#include "optimize.h"

namespace py = pybind11;

int py_steepest(const SteepestOptions *options, 
                const LineSearchOptions *lsoptions,
                py::array_t<double, py::array::c_style | py::array::forcecast> &x,
                const void *params,
                const ObjectiveFunction &func){
    py::buffer_info info = x.request();
    int n = info.size;
    double *xptr = (double*)info.ptr;
    return steepest(options, lsoptions, n, xptr, params, func);
}

PYBIND11_MODULE(optimize, m){
    py::class_<LineSearchOptions>(m, "LineSearchOptions")
        .def(py::init<>())
        .def_readwrite("Tolf", &LineSearchOptions::Tolf)
        .def_readwrite("Tolg", &LineSearchOptions::Tolg)
        .def_readwrite("Tolx", &LineSearchOptions::Tolx)
        .def_readwrite("Min_step", &LineSearchOptions::Min_step)
        .def_readwrite("Max_step", &LineSearchOptions::Max_step)
        .def_readwrite("Xtrapu", &LineSearchOptions::Xtrapu)
        .def_readwrite("Xtrapl", &LineSearchOptions::Xtrapl);

    py::class_<SteepestOptions>(m, "SteepestOptions")
        .def(py::init<>())
        .def_readwrite("Tolg", &SteepestOptions::Tolg)
        .def_readwrite("Max_neval", &SteepestOptions::Max_neval);

    py::class_<NLCGOptions>(m, "NLCGOptions")
        .def(py::init<>())
        .def_readwrite("Tolg", &NLCGOptions::Tolg)
        .def_readwrite("Max_neval", &NLCGOptions::Max_neval)
        .def_readwrite("Beta_choice", &NLCGOptions::Beta_choice);

    m.def("py_steepest", &py_steepest);
}

