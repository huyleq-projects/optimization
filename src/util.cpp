#include <cstdio>
#include <lapacke.h>
#include "util.h"

void print(const int n, const double *x){
    for(int i = 0; i < n; i++) printf("%8e ", x[i]);
    printf("\n"); fflush(stdout);
    return;
}

void print(const int n, const int *x){
    for(int i = 0; i < n; i++) printf("%d ", x[i]);
    printf("\n"); fflush(stdout);
    return;
}

void print(const int m, const int n, const double *a){
    for(int i = 0; i < m; i++){
        for(int j = 0; j < n; j++){
            if(a[i+j*m] < 0) printf("%e ", a[i+j*m]);
            else printf(" %e ", a[i+j*m]);
        }
        printf("\n");
    }
    fflush(stdout);
    return;
}

void random_vector(const int m, double *a, const int d){
    srand(time(NULL));
    int seed[4]={rand()%MAXSEED+1, 1, 1, 1};
    LAPACKE_dlarnv(d, seed, m, a);
    return;
}

