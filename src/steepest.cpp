#include <cstring>
#include <cmath>

#include <cblas.h>
#include <glog/logging.h>

#include "util.h"
#include "optimize.h"

int steepest(const SteepestOptions *options, 
             const LineSearchOptions *lsoptions,
             const int n, double *x, const void *params,
             const ObjectiveFunction &func){
    double tolg = options->Tolg;
    int max_neval = options->Max_neval;

    double *g = new double[n]; // gradient
    double *p = new double[n]; // search direction

    int stop = 0, iter = 0, neval = 0;
    
    double f = func(n, x, g, params); 
    neval++;

    // step size guess
    double gnorm = cblas_dnrm2(n, g, 1);
    double stp = 1/gnorm;

    printf("iter  neval         f           gnorm\n");
    const char* format = "% 4d  % 4d    % 8e   % 3.2e\n";

    while(!stop){
        iter++;
        printf(format, iter, neval, f, gnorm); fflush(stdout);

        // stopping criteria
        if(neval >= max_neval){
            stop = 1;
            VLOG(0) << "Maximum number of iterations reached." << std::endl;
            break;
        }

        double xnorm = cblas_dnrm2(n, x, 1);
        xnorm = std::max(xnorm, double(1));
        if(gnorm/xnorm <= tolg){
            stop = 2;
            VLOG(0) << "Gradient is too small. Local minimum reached." << std::endl;
            break;
        }
        
        // compute search direction
        cblas_daxpby(n, -1, g, 1, 0, p, 1);
        
        // line search
        int info = line_search(lsoptions, n, stp, f, g, p, x, params, neval, max_neval, func);
        if(info != 1){
            stop = 3;
            VLOG(1) << "Line search returns error code " << info << std::endl;
            break;
        }
        
        // update gnorm
        gnorm = cblas_dnrm2(n, g, 1);
    }

    return stop;
}
