#ifndef UTIL_H
#define UTIL_H

#define MAXSEED 4093

template <typename T> int sgn(T val){
    return (T(0) < val) - (val < T(0));
}

void print(const int n, const double *x);
void print(const int n, const int *x);
void print(const int m, const int n, const double *a);
void random_vector(const int m, double *a, const int d);

#endif
