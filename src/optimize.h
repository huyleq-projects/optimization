#ifndef OPTIMIZE_H
#define OPTIMIZE_H

#include <functional>

using ObjectiveFunction = std::function<double(
                                        const int, 
                                        const double *, 
                                        double *, 
                                        const void*)>;

struct LineSearchOptions{
    double Tolf = 1e-4, Tolg = 0.9, Tolx = 1e-16; 
    double Min_step = 1e-10, Max_step = 1e10;
    double Xtrapu = 4, Xtrapl = 1.1;
};

struct SteepestOptions{
    double Tolg = 1e-6;
    int Max_neval = 100;
};

struct NLCGOptions{
    double Tolg = 1e-6;
    int Max_neval = 100;
    int Beta_choice = 0;
};

int line_search(const LineSearchOptions *lsoptions, 
                 const int n, double &stp, double &f, 
                 double *g, const double *p, double *x, 
                 const void *params, 
                 int &neval, const int max_neval,
                 const ObjectiveFunction &func);

void safe_guard(double &stx, double &fx, double &gx, 
                double &sty, double &fy, double &gy, 
                double &stp, double f, double gp, 
                bool &brackt, double stmin, double stmax);

double cubic_minimizer(double x0, double f0, double g0, 
                       double x1, double f1, double g1,
                       double xmin=-1, double xmax=-1);

double quadratic_minimizer0(double x0, double f0, double g0, 
                            double x1, double f1);

double quadratic_minimizer1(double x0, double g0, 
                            double x1, double g1);

int steepest(const SteepestOptions *options, 
              const LineSearchOptions *lsoptions,
              const int n, double *x,const void *params,
              const ObjectiveFunction &func);

int nlcg(const NLCGOptions *options, 
         const LineSearchOptions *lsoptions,
         const int n, double *x,const void *params,
         const ObjectiveFunction &func);

#endif
