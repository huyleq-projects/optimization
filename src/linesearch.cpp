#include <cstring>
#include <cmath>

#include <cblas.h>
#include <glog/logging.h>

#include "optimize.h"

#define DELTA 0.66

int line_search(const LineSearchOptions *lsoptions, 
                 const int n, double &stp, double &f, 
                 double *g, const double *p, double *x, 
                 const void *params, 
                 int &neval, const int max_neval,
                 const ObjectiveFunction &func){
    double ftol = lsoptions->Tolf;
    double gtol = lsoptions->Tolg;
    double xtol = lsoptions->Tolx;
    double stpmin = lsoptions->Min_step;
    double stpmax = lsoptions->Max_step;
    double xtrapu = lsoptions->Xtrapu;
    double xtrapl = lsoptions->Xtrapl;

    int info = 0;
    
    VLOG(1) << "Initial guess for step " << stp << std::endl;

    if(stp < stpmin){
        VLOG(1) << "Step is smaller than minimum step" << std::endl;
        return (info = -1);
    }
    
    if(stp > stpmax){
        VLOG(1) << "Step is larger than maximumstep" << std::endl;
        return (info = -2);
    }
    
    if(stpmin > stpmax){
        VLOG(1) << "Minimum step is larger than maximumstep" << std::endl;
        return (info = -3);
    }
    
    if(stpmin < 0){
        VLOG(1) << "Negative minimum step" << std::endl;
        return (info = -4);
    }
    
    double gp = cblas_ddot(n, g, 1, p, 1);
    if(gp > 0){
        VLOG(1) << "Search direction is not a descent direction" << std::endl;
        return (info = -5);
    }
    
    double *x0 = new double[n];
    std::memcpy(x0, x, n*sizeof(double));

    bool brackt = false;
    bool stage1 = true;
    double finit = f;
    double ginit = gp;
    double gtest = ftol*ginit;
    double width = stpmax - stpmin;
    double width1 = 2*width;
       
    double stx = 0, fx = finit, gx = ginit;
    double sty = 0, fy = finit, gy = ginit;
    double stmin = 0, stmax = stp + xtrapu*stp;

    while(!info){
        VLOG(1) << "Trial step " << stp << std::endl;
        std::memcpy(x, x0, n*sizeof(double));
        cblas_daxpy(n, stp, p, 1, x, 1);
        
        f = func(n, x, g, params);
        neval++;
        
        if((brackt && (stp <= stmin || stp >= stmax))){
            VLOG(1) << "Line search fails due to rounding error."
                    << "\nstp = " << stp << " stmin = " << stmin << " stmax = " << stmax 
                    << "\nTolerances might be too small" << std::endl;
            return (info = 6);
        }
        
        if(brackt && (stmax - stmin) <= xtol*stmax){
            VLOG(1) << "X-tolerance satisfied" << std::endl;
            return (info = 5);
        }
        
        double ftest = finit + stp*gtest;
        gp = cblas_ddot(n, g, 1, p, 1);

        if(stp ==stpmax && f <= ftest && gp <= gtest){
            VLOG(1) << "Step = maximum step" << std::endl;
            return (info = 4);
        }

        if(stp ==stpmin && (f <= ftest || gp <= gtest)){
            VLOG(1) << "Step = minimum step" << std::endl;
            return (info = 3);
        }

        if(neval >= max_neval){
            VLOG(1) << "Maximum number of evaluations reached" << std::endl;
            return (info = 2);
        }

        if(f <= ftest && std::fabs(gp) <= gtol*(-ginit)){
            VLOG(1) << "Found satisfactory step " << stp << std::endl;
            return (info = 1);
        }

        if(stage1 && f <= ftest && gp >= std::min(ftol, gtol)*ginit) stage1 = false;
        
        VLOG(1) << "Before calling safe_guard:"
                << "\nstx = " << stx << " sty = " << sty
                << "\nstmin = " << stmin << " stmax = " << stmax
                << "\nstp = " << stp << std::endl;

        if(stage1 && f <= fx && f > ftest){
            double fm = f - stp*gtest, gm = gp - gtest;
            double fxm = fx - stx*gtest, gxm = gx - gtest;
            double fym = fy - sty*gtest, gym = gy - gtest;

            safe_guard(stx, fxm, gxm, sty, fym, gym, stp, fm, gm, brackt, stmin, stmax);
            
            fx = fxm + stx*gtest; gx = gxm + gtest;
            fy = fym + sty*gtest; gy = gym + gtest;
        }
        else safe_guard(stx, fx, gx, sty, fy, gy, stp, f, gp, brackt, stmin, stmax);
        
        VLOG(1) << "After calling safe_guard:"
                << "\nstx = " << stx << " sty = " << sty
                << "\nstmin = " << stmin << " stmax = " << stmax
                << "\nstp = " << stp << std::endl;

        if(brackt){
            VLOG(1) << "Trial step is bracketed" << std::endl;
            if(std::fabs(sty - stx) >= DELTA*width1) stp = stx + (sty - stx)/2;
            width1 = width; width = std::fabs(sty - stx);
            stmin = std::min(stx, sty);
            stmax = std::max(stx, sty);
        }
        else{
            VLOG(1) << "Trial step is NOT bracketed" << std::endl;
            stmin = stp + xtrapl*(stp - stx);
            stmax = stp + xtrapu*(stp - stx);
        }

        stp = std::min(stp, stpmax);
        stp = std::max(stp, stpmin);

        if((brackt && (stp <= stmin || stp >= stmax)) ||
           (brackt && (stmax - stmin) <= xtol*stmax)){
            VLOG(1) << "Safe guard cannot make further progress\n"
                    << "Chooing best step " << stx << std::endl;
            stp = stx;
        }
    }
    
    delete []x0;
    return info;
}

void safe_guard(double &stx, double &fx, double &gx, 
                double &sty, double &fy, double &gy, 
                double &stp, double f, double gp, 
                bool &brackt, double stmin, double stmax){
    double gxgp = gx*gp, stpf;

    if(f > fx){
        double stpc = cubic_minimizer(stp, f, gp, stx, fx, gx);
        double stpq = quadratic_minimizer0(stx, fx, gx, stp, f);
        if(std::fabs(stpc - stx) < std::fabs(stpq - stx)) stpf = stpc;
        else stpf = stpc + (stpq - stpc)/2;
        brackt = true;
    }
    else if(gxgp < 0){
        double stpc = cubic_minimizer(stx, fx, gx, stp, f, gp);
        double stpq = quadratic_minimizer1(stx, gx, stp, gp);
        if(std::fabs(stpc - stp) > std::fabs(stpq - stp)) stpf = stpc;
        else stpf = stpq;
        brackt = true;
    }
    else if(std::fabs(gp) < std::fabs(gx)){
        double stpc = cubic_minimizer(stx, fx, gx, stp, f, gp, stmin, stmax);
        double stpq = quadratic_minimizer1(stx, gx, stp, gp);
        
        if(brackt){
            if(std::fabs(stpc - stp) < std::fabs(stpq - stp)) stpf = stpc;
            else stpf = stpq;
            if(stp > stx) stpf = std::min(stp + DELTA*(sty - stp), stpf);
            else stpf = std::max(stp + DELTA*(sty - stp), stpf);
        }
        else{
            if(std::fabs(stpc - stp) > std::fabs(stpq - stp)) stpf = stpc;
            else stpf = stpq;
            stpf = std::min(stpf, stmax);
            stpf = std::max(stpf, stmin);
        }
    }
    else{
        if(brackt) stpf = cubic_minimizer(sty, fy, gy, stp, f, gp);
        else if(stp > stx) stpf = stmax;
        else stpf = stmin;
    }

    if(f > fx){
        sty = stp; fy = f; gy = gp;
    }
    else{
        if(gxgp < 0){
            sty = stx; fy = fx; gy = gx;
        }
        stx = stp; fx = f; gx = gp;
    }
    
    stp = stpf;
    return;
}

double cubic_minimizer(double x0, double f0, double g0, 
                       double x1, double f1, double g1,
                       double xmin, double xmax){
    double theta = g0 + g1 - 3*(f0 - f1)/(x0 - x1);
    double s = std::max(std::fabs(theta), std::max(std::fabs(g0), std::fabs(g1)));
    double gamma = s*std::sqrt(std::max(double(0), (theta/s)*(theta/s) - (g0/s)*(g1/s)));
    if(x0 < x1) gamma = -gamma;
    double p = gamma - g1 + theta;
    double q = 2*gamma  - g1 + g0;
    double r = p/q;
    double xc = x1 + r*(x0 - x1);
    if(xmin >= 0 && xmax >= 0){
        if(r < 0 && gamma != 0) return xc;
        else if(x1 > x0) return xmax;
        else return xmin;
    }
    else return xc;
}

double quadratic_minimizer0(double x0, double f0, double g0, 
                            double x1, double f1){
    return x0 + (x1 - x0)*g0/2/(g0 - (f1 - f0)/(x1 - x0));
}

double quadratic_minimizer1(double x0, double g0, 
                            double x1, double g1){
    return x1 - g1*(x1 - x0)/(g1 - g0);
}
