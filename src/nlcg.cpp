#include <cstring>
#include <cmath>

#include <cblas.h>
#include <glog/logging.h>

#include "util.h"
#include "optimize.h"

int nlcg(const NLCGOptions *options, 
         const LineSearchOptions *lsoptions,
         const int n, double *x, const void *params,
         const ObjectiveFunction &func){
    double tolg = options->Tolg;
    int max_neval = options->Max_neval;
    int beta_choice = options->Beta_choice;

    double *g = new double[n]; // gradient
    double *p = new double[n]; // search direction
    double *g_old = new double[n]; // previous gradient

    int stop = 0, iter = 0, neval = 0;
    
    // initial objective function and gradient
    double f = func(n, x, g, params); 
    neval++;

    // compute search direction
    cblas_daxpby(n, -1, g, 1, 0, p, 1);
        
    // step size guess
    double gnorm = cblas_dnrm2(n, g, 1);
    double gnorm2 = gnorm*gnorm;
    double stp = 1/gnorm;

    printf("iter  neval         f           gnorm\n");
    const char* format = "% 4d  % 4d    % 8e   % 3.2e\n";

    while(!stop){
        iter++;
        printf(format, iter, neval, f, gnorm); fflush(stdout);

        // stopping criteria
        if(neval >= max_neval){
            stop = 1;
            VLOG(0) << "Maximum number of iterations reached." << std::endl;
            break;
        }

        double xnorm = cblas_dnrm2(n, x, 1);
        xnorm = std::max(xnorm, double(1));
        if(gnorm/xnorm <= tolg){
            stop = 2;
            VLOG(0) << "Gradient is too small. Local minimum reached." << std::endl;
            break;
        }
        
        // save the previous gradient
        std::memcpy(g_old, g, n*sizeof(double));

        // line search
        int info = line_search(lsoptions, n, stp, f, g, p, x, params, neval, max_neval, func);
        if(info != 1){
            stop = 3;
            VLOG(1) << "Line search returns error code " << info << std::endl;
            break;
        }
        
        // compute beta
        cblas_daxpby(n, 1, g, 1, -1, g_old, 1);    // g_new - g_old
        double gg = cblas_ddot(n, g, 1, g_old, 1); // g_new^T(g_new - g_old)
        double beta_PR = gg/gnorm2; 
        double beta;
        if(beta_choice == 0) beta = beta_PR;

        // compute new search direction
        double theta = cblas_ddot(n, g, 1, p, 1)/gnorm2; 
        cblas_daxpby(n, -1, g, 1, beta, p, 1);    // p_new = -g_new + beta*p_old
        cblas_daxpy(n, -theta, g_old, 1, p, 1);   // p_new = -g_new + beta*p_old - theta*(g_new - g_old)

        // update gnorm
        gnorm = cblas_dnrm2(n, g, 1);
        gnorm2 = gnorm*gnorm;
    }

    return stop;
}
