/* 
 * Reference: Testing unconstrained optimization software,
 * More et al, 1981, ACM Transactions on Mathematical Software,
 * Vol 7, No 1, 17-41
 */

#include <cmath>
#include "functions.h"

double quad(const int n, const double *x, double *g, const void *params){
    double f = (x[0] - 1)*(x[0] - 1) + 100*(x[1] - 2)*(x[1] - 2);
    if(g){
        g[0] = 2*(x[0] - 1);
        g[1] = 200*(x[1] - 2);
    }
    return f;
}

double rosenbrock(const int n, const double *x, double *g, const void *params){
    double f0 = 10*(x[1] - x[0]*x[0]);
    double f1 = 1 - x[0];
    double f = f0*f0 + f1*f1;
    if(g){
        g[0] = -40*f0*x[0] - 2*f1;
        g[1] = 20*f0;
    }
    return f;
}

double badpowell(const int n, const double *x, double *g, const void *params){
    double f0 = 1e4*x[0]*x[1] - 1;
    double f1 = std::exp(-x[0]) + std::exp(-x[1]) - 1.0001;
    double f = f0*f0 + f1*f1;
    if(g){
        g[0] = 2e4*f0*x[1] - 2*f1*std::exp(-x[0]);
        g[1] = 2e4*f0*x[0] - 2*f1*std::exp(-x[1]);
    }
    return f;
}

double beale(const int n, const double *x, double *g, const void *params){
    double t0 = 1.500 - x[0] + x[0]*x[1];
    double t1 = 2.250 - x[0] + x[0]*x[1]*x[1];
    double t2 = 2.625 - x[0] + x[0]*x[1]*x[1]*x[1];
    double f = t0*t0 + t1*t1 + t2*t2;
    if(g){
        g[0] = 2*(t0*(x[1] - 1) + t1*(x[1]*x[1] - 1) + t2*(x[1]*x[1]*x[1] - 1));
        g[1] = 2*(t0*x[0] + t1*2*x[0]*x[1] + t2*3*x[0]*x[1]*x[1]);
    }
    return f;
}

double badbrown(const int n, const double *x, double *g, const void *params){
    double f0 = x[0] - 1e6;
    double f1 = x[1] - 2e-6;
    double f2 = x[0]*x[1] - 2;
    double f = f0*f0 + f1*f1 + f2*f2;
    if(g){
        g[0] = 2*f0 + 2*f2*x[1];
        g[1] = 2*f1 + 2*f2*x[0];
    }
    return f;
}

double powell(const int n, const double *x, double *g, const void *params){
    double f0 = x[0] + 10*x[1];
    double f1 = std::sqrt(5.)*(x[2] - x[3]);
    double f2 = x[1] - 2*x[2]; f2 = f2*f2;
    double f3 = x[0] - x[3]; f3 = std::sqrt(10.)*f3*f3;
    double f = f0*f0 + f1*f1 + f2*f2 + f3*f3;
    if(g){
        g[0] = 2*f0 + 2*f3*2*std::sqrt(10.)*(x[0] - x[3]);
        g[1] = 2*f0*10 + 2*f2*2*(x[1] - 2*x[2]);
        g[2] = 2*f1*std::sqrt(5.) - 2*f2*2*(x[1] - 2*x[2])*2;
        g[3] = -2*f1*std::sqrt(5.) - 2*f3*2*std::sqrt(10.)*(x[0] - x[3]);
    }
    return f;
}

double *init_function(std::string &func_opt, int &n, ObjectiveFunction &func){
    double *x;
    if(func_opt.compare("quad") == 0){
        n = 2;
        x = new double[n];
        x[0] = -1; x[1] = -0.5;
        func = quad;
    }
    else if(func_opt.compare("rosenbrock") == 0){
        n = 2;
        x = new double[n];
        x[0] = -1.2; x[1] = 1;
        func = rosenbrock;
    }
    else if(func_opt.compare("badpowell") == 0){
        n = 2;
        x = new double[n];
        x[0] = 0; x[1] = 1;
        func = badpowell;
    }
    else if(func_opt.compare("beale") == 0){
        n = 2;
        x = new double[n];
        x[0] = x[1] = 1;
        func = beale;
    }
    else if(func_opt.compare("badbrown") == 0){
        n = 2;
        x = new double[n];
        x[0] = x[1] = 1;
        func = badbrown;
    }
    else if(func_opt.compare("powell") == 0){
        n = 4;
        x = new double[n];
        x[0] = 3; x[1] = -1; x[2] = 0; x[3] = 1; 
        func = powell;
    }
    return x;
}
