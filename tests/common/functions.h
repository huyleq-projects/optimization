#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>
#include <functional>

using ObjectiveFunction = std::function<double(
                                        const int, 
                                        const double *, 
                                        double *, 
                                        const void*)>;

double quad(const int n, const double *x, double *g, const void *params);
double rosenbrock(const int n, const double *x, double *g, const void *params);
double badpowell(const int n, const double *x, double *g, const void *params);
double beale(const int n, const double *x, double *g, const void *params);
double badbrown(const int n, const double *x, double *g, const void *params);
double powell(const int n, const double *x, double *g, const void *params);

double *init_function(std::string &func_opt, int &n, ObjectiveFunction &func);

#endif
