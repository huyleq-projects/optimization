#include <cstdio>
#include <cstdlib>
#include <string>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "cblas.h"

#include "util.h"
#include "functions.h"

using namespace std;

int main(int argc,char **argv){
    GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
   
    if(argc != 3){
        LOG(INFO) << "./test_function.x function_name number_of_iteration" << std::endl;
        return 0;
    }

    int n;
    ObjectiveFunction func;
    std::string func_opt = std::string(argv[1]);
    double *x0 = init_function(func_opt, n, func);
    
    double *x = new double[n];
    double *g0 = new double[n];
    double *dx = new double[n];
    
    double f0 = func(n, x0, g0, nullptr);

    random_vector(n, dx, 1);
    double normdx = cblas_dnrm2(n, dx, 1);
    cblas_dscal(n, 1/normdx, dx, 1);

    double h = 1, e0 = 0;
    int m = atoi(argv[2]);
    for(int i = 0; i < m; i++){
        std::memcpy(x, x0, n*sizeof(double));
        cblas_daxpy(n, h, dx, 1, x, 1);
        double f = func(n, x, nullptr, nullptr);
        double e = f - f0 - h*cblas_ddot(n, dx, 1, g0, 1);
        printf("h %e e %e should be %e\n", h, e, e0/4);
        e0 = e; h /= 2;
    }

    delete []x0; delete []x; delete []g0; delete []dx;
    return 0;
}

