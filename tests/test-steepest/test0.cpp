#include <cstdio>
#include <cstdlib>
#include <string>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "optimize.h"
#include "util.h"
#include "functions.h"

using namespace std;

int main(int argc,char **argv){
    GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    
    if(argc != 5){
        LOG(INFO) << "./test0 function_name method_name niter gtol" << std::endl;
        return 0;
    }

    int n;
    ObjectiveFunction func;
    std::string func_opt = std::string(argv[1]);
    double *x = init_function(func_opt, n, func);
    
    LineSearchOptions lsoptions;
    int niter = atoi(argv[3]);
    double tolg = atof(argv[4]);

    std::string method_opt = std::string(argv[2]);
    if(method_opt.compare("steepest") == 0){
        SteepestOptions options;
        options.Max_neval = niter;
        options.Tolg = tolg;
        steepest(&options, &lsoptions, n, x, nullptr, func);
    }
    else if(method_opt.compare("nlcg") == 0){
        NLCGOptions options;
        options.Max_neval = niter;
        options.Tolg = tolg;
        nlcg(&options, &lsoptions, n, x, nullptr, func);
    }

    printf("Final solution\n");
    print(n, x);

    delete []x;
    return 0;
}

