#!/bin/zsh

mkdir build
cd build
cmake ..
make
for func (quad rosenbrock badpowell beale badbrown powell)
do
    logfilename="log_${func}_steepest.txt"
    GLOG_logtostderr=1 GLOG_v=0 ./test0 $func steepest 100 1e-5 >& $logfilename
    logfilename="log_${func}_nlcg.txt"
    GLOG_logtostderr=1 GLOG_v=0 ./test0 $func nlcg 100 1e-5 >& $logfilename
done
