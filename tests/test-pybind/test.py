import sys
sys.path.append("/Users/huyle/projects/optimization/src/build")
import numpy as np
import optimize

def quad(n, x_, g, params):
    x = np.ctypeslib.as_array(x_)
    f = (x[0] - 1)*(x[0] - 1) + 100*(x[1] - 2)*(x[1] - 2)
    if g:
        g[0] = 2*(x[0] - 1)
        g[1] = 200*(x[1] - 2)
    return f

st_option = optimize.SteepestOptions()
ls_option = optimize.LineSearchOptions()
n = 2
x = np.empty((n,), dtype=np.double)
x[0] = -1; x[1] = -0.5

optimize.py_steepest(st_option, ls_option, x, None, quad)

