#include <cstdio>
#include <cstdlib>

#include "util.h"

#include "simplex.h"

int main(int argc, char **argv){
    int m = 2, n = 2, mn = m*n, mpn = m+n, n1 = n+1;
    double *a = new double[mn];
    double *b = new double[m];
    double *c = new double[n1];
    int *p = new int[mpn]();
    
    a[0] = 2; a[2] = -1;
    a[1] = 1; a[3] = -5;

    b[0] = 2; b[1] = -4;

    c[0] = 2; c[1] = -1; c[2] = 0;

    init_simplex(a, b, c, p, m, n);
    printf("a:\n"); print(m, n, a);
    printf("b:\n"); print(m, b);
    printf("c:\n"); print(n+1, c);
    printf("p:\n"); print(m+n, p);
    printf("\n");

    delete []a; delete []b; delete []c; delete []p;
    return 0;
}
