#include <cstdio>
#include <cstdlib>

#include "util.h"

#include "simplex.h"

void test0();
void test1();

int main(int argc, char **argv){
    if(argc != 2)
        printf("Please provide a test option 0 or 1. ./test_simplex.x option\n");
    else if(argv[1][0] == '0') test0();
    else if(argv[1][0] == '1') test1();
    else printf("option has to be either 0 or 1\n");
    return 0;
}

void test0(){
    int m = 3, n = 3, mn = m*n, mpn = m+n, n1 = n+1;
    double *a = new double[mn];
    double *b = new double[m];
    double *c = new double[n1];
    double *x = new double[n];

    a[0] = 1; a[3] = 1; a[6] = 3;
    a[1] = 2; a[4] = 2; a[7] = 5;
    a[2] = 4; a[5] = 1; a[8] = 2;

    b[0] = 30; b[1] = 24; b[2] = 36;

    c[0] = 3; c[1] = 1; c[2] = 2; c[3] = 0;

    int r = simplex(x, a, b, c, m, n);
    switch(r){
        case 1:
        printf("optimal objective function %e\noptimal solution:\n", c[n]);
        print(n, x);
        break;
        case 0:
        printf("infeasible\n");
        break;
        case -1:
        printf("unbounded\n");
        break;
        default:
        printf("something is wrong\n");
    }

    delete []a; delete []b; delete []c; delete []x;
    return;
}

void test1(){
    int m = 2, n = 2, mn = m*n, mpn = m+n, n1 = n+1;
    double *a = new double[mn];
    double *b = new double[m];
    double *c = new double[n1];
    double *x = new double[n];
    
    a[0] = 2; a[2] = -1;
    a[1] = 1; a[3] = -5;

    b[0] = 2; b[1] = -4;

    c[0] = 2; c[1] = -1; c[2] = 0;

    int r = simplex(x, a, b, c, m, n);
    switch(r){
        case 1:
        printf("optimal objective function %e\noptimal solution:\n", c[n]);
        print(n, x);
        break;
        case 0:
        printf("infeasible\n");
        break;
        case -1:
        printf("unbounded\n");
        break;
        default:
        printf("something is wrong\n");
    }

    delete []a; delete []b; delete []c; delete []x;
    return;
}
