#include <cstdio>
#include <cstdlib>

#include "util.h"

#include "simplex.h"

int main(int argc, char **argv){
    int m = 3, n = 3, mn = m*n, mpn = m+n, n1 = n+1;
    double *a = new double[mn];
    double *b = new double[m];
    double *c = new double[n1];
    int *p = new int[mpn]();
    
    a[0] = 1; a[3] = 1; a[6] = 3;
    a[1] = 2; a[4] = 2; a[7] = 5;
    a[2] = 4; a[5] = 1; a[8] = 2;

    b[0] = 30; b[1] = 24; b[2] = 36;

    c[0] = 3; c[1] = 1; c[2] = 2; c[3] = 0;

    p[0] = 0; p[1] = 1; p[2] = 2; p[3] = 3; p[4] = 4; p[5] = 5;
    
    int l = 2, e = 0;
    pivot(a, b, c, p, m, n, l, e);
    printf("a:\n"); print(m, n, a);
    printf("b:\n"); print(m, b);
    printf("c:\n"); print(n1, c);
    printf("p:\n"); print(mpn, p);
    printf("\n");

    l = 1; e = 2;
    pivot(a, b, c, p, m, n, l, e);
    printf("a:\n"); print(m, n, a);
    printf("b:\n"); print(m, b);
    printf("c:\n"); print(n1, c);
    printf("p:\n"); print(mpn, p);
    printf("\n");

    l = 1; e = 1;
    pivot(a, b, c, p, m, n, l, e);
    printf("a:\n"); print(m, n, a);
    printf("b:\n"); print(m, b);
    printf("c:\n"); print(n1, c);
    printf("p:\n"); print(mpn, p);
    printf("\n");

    delete []a; delete []b; delete []c; delete []p;
    return 0;
}
