#include <cblas.h>
#include "gflags/gflags.h"
#include "glog/logging.h"
#include "dogleg.h"

double badpowell(const int m, const int n, const double *x, 
              double *r, double *g, double *jac);

void run_test(double x1_, double x2_);

int main(int argc, char** argv) {
    GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    
    double x1 = 0;
    double x2 = 1;
    
    run_test(x1, x2);
    
    return 0;
}

double badpowell(const int m, const int n, const double *x, 
              double *r, double *g, double *jac){
    r[0] = 1e4*x[0]*x[1] - 1.;
    r[1] = exp(-x[0]) + exp(-x[1]) - 1.0001;
    
    if(g && jac){
        jac[0] = 1e4*x[1]; jac[1] = 1e4*x[0];
        jac[2] = -exp(-x[0]); jac[3] = -exp(-x[1]);
        
        cblas_dgemv(CblasRowMajor, CblasTrans, n, n, 1, jac, n, r, 1, 0, g, 1); // J^T*r
    }
    
    return cblas_ddot(n, r, 1, r, 1)/2;
}

void run_test(double x1_, double x2_){
    DoglegOptions options;
    options.Maxeval = 200;

    int n=2;
    double *x = new double[n];
    x[0] = x1_;
    x[1] = x2_;
    
    printf("\nDogleg:\n");
    printf("Initial: x1 = %g x2 = %g\n", x[0], x[1]);
    
    std::function<double(const int, const int, const double *,
                         double *, double *, double *)> func = badpowell;

    dogleg(options, n, n, x, func);
    
    printf("Final: x1 = %g x2 = %g\n", x[0], x[1]);
    
    delete []x;
    return;
}

