#include <cblas.h>
#include "gflags/gflags.h"
#include "glog/logging.h"
#include "dogleg.h"

double badbrown(const int m, const int n, const double *x, 
              double *r, double *g, double *jac);

void run_test(double x1_, double x2_);

int main(int argc, char** argv) {
    GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    
    double x1 = 1;
    double x2 = 1;
    
    run_test(x1, x2);
    
    return 0;
}

double badbrown(const int m, const int n, const double *x, 
              double *r, double *g, double *jac){
    r[0] = x[0] - 1e6;
    r[1] = x[1] - 2e-6;
    r[2] = x[0]*x[1] - 2.;
    
    if(g && jac){
        jac[0] = 1;    jac[1] = 0;
        jac[2] = 0;    jac[3] = 1;
        jac[4] = x[1]; jac[5] = x[0];
        
        cblas_dgemv(CblasRowMajor, CblasTrans, m, n, 1, jac, n, r, 1, 0, g, 1); // J^T*r
    }
    
    return cblas_ddot(m, r, 1, r, 1)/2;
}

void run_test(double x1_, double x2_){
    DoglegOptions options;
    options.Maxeval = 200;

    int m=3, n=2;
    double *x = new double[n];
    x[0] = x1_;
    x[1] = x2_;
    
    printf("\nDogleg:\n");
    printf("Initial: x1 = %g x2 = %g\n", x[0], x[1]);
    
    std::function<double(const int, const int, const double *,
                         double *, double *, double *)> func = badbrown;

    dogleg(options, m, n, x, func);
    
    printf("Final: x1 = %g x2 = %g\n", x[0], x[1]);
    
    delete []x;
    return;
}

