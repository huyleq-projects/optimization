#include <cblas.h>
#include "dogleg.h"

double powell(const int m, const int n, const double *x, 
              double *r, double *g, double *jac);

void run_test(double x1_, double x2_, double x3_, double x4_);

int main(int argc, char** argv) {

  double x1 = 3.0;
  double x2 = -1.0;
  double x3 = 0.0;
  double x4 = 1.0;

  run_test(x1, x2, x3, x4);

  return 0;
}

double powell(const int m, const int n, const double *x, 
              double *r, double *g, double *jac){
    r[0] = x[0] + 10*x[1];
    r[1] = sqrt(5.)*(x[2] - x[3]);
    r[2] = x[1] - 2*x[2]; r[2] *= r[2];
    r[3] = x[0] - x[3]; r[3] *= (sqrt(10.)*r[3]);
    
    if(g && jac){
        std::memset(jac, 0, n*n*sizeof(double));
        jac[0] = 1; jac[1] = 10;
        jac[6] = sqrt(5.); jac[7] = -jac[6];
        jac[9] = 2*(x[1] - 2*x[2]); jac[10] = -2*jac[9];
        jac[12] = 2*sqrt(10.)*(x[0] - x[3]); jac[15] = -jac[12];
        
        cblas_dgemv(CblasRowMajor, CblasTrans, m, n, 1, jac, n, r, 1, 0, g, 1); // J^T*r
    }
    
    return cblas_ddot(m, r, 1, r, 1)/2;
}

void run_test(double x1_, double x2_, double x3_, double x4_){
    DoglegOptions options;

    int n=4;
    double *x = new double[n];
    x[0] = x1_;
    x[1] = x2_;
    x[2] = x3_;
    x[3] = x4_;
    
    printf("\nDogleg:\n");
    printf("Initial: x1 = %g x2 = %g x3 = %g x4 = %g\n", x[0], x[1], x[2], x[3]);
    
    std::function<double(const int, const int, const double *,
                         double *, double *, double *)> func = powell;

    dogleg(options, n, n, x, func);
    
    printf("Final: x1 = %g x2 = %g x3 = %g x4 = %g\n", x[0], x[1], x[2], x[3]);
    
    delete []x;
    return;
}

